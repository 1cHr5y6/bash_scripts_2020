#!/bin/bash

function reEnter_username() {
	unset username
	unset password

	echo -n "Enter username: "
	read username
}

function re_enter() {	
	echo ""
	prompt="Enter passphrase: "
	while IFS= read -p "$prompt" -r -s -n 1 char
	do
		if [[ $char == $'\0' ]]; then
			break
		fi
		prompt="*"
		password+="$char"
	done
}

function authentication2() {
	echo ""
	# Uncomment line below to use for debugging
	#echo "DEBUG: authentication2()"
	reEnter_username
	prompt2="Enter passphrase: "
	while IFS= read -p "$prompt2" -r -s -n 1 char
	do
		if [[ $char == $'\0' ]]; then
			break
		fi
		prompt2="*"
		password2+="$char"
	done
	echo ""
	if [[ ( $password2 == 8392 ) && ( $username == "Alice" ) ]]; then
	echo ""
       	echo "Hello, Alice."
elif [[ ( $password2 == 2383 ) && ( $username == "Bob" ) ]]; then
	echo ""
	echo "Hello, Bob."
elif [[ ( $password2 == 7389 ) && ( $username == "Jane" ) ]]; then
	echo ""
	echo "Hello, Jane."
else
	echo "Invalid username and/or password."
	authentication3

fi
}

function authentication3() {
	echo ""
	# Uncomment line below to use for debugging
	#echo "DEBUG: authentication3"
	reEnter_username
	prompt3="Enter passphrase: "
	while IFS= read -p "$prompt3" -r -s -n 1 char
	do
		if [[ $char == $'\0' ]]; then
			break
		fi
		prompt3="*"
		password3+="$char"
	done
	echo ""
	# Uncomment line below to use for debugging
	#echo "DEBUG: function authentication3"
	if [[ ( $password3 == 8392 ) && ( $username == "Alice" ) ]]; then
	echo ""
       	echo "Hello, Alice."
elif [[ ( $password3 == 2383 ) && ( $username == "Bob" ) ]]; then
	echo ""
	echo "Hello, Bob."
elif [[ ( $password3 == 7389 ) && ( $username == "Jane" ) ]]; then
	echo ""
	echo "Hello, Jane."
else
	echo "Invalid username and/or password."
	echo "Exiting program..."
	echo ""
fi
}

function menu_list() {
	echo ""
	# Uncomment line below to use for debugging
	#echo "DEBUG: menu_list"
	echo "Make a selection from the Office Supply Menu"
	echo ""
	echo "1. Order box of copy paper (10 reams)"
	echo "2. Order box of black pens (20 count)"
	echo "3. Order box of legal pads (15 count)"
	echo "4. Exit program"
	echo ""
	read input
}

function menu_list_final() {
	echo ""
	# Uncomment line below to use for debugging
	#echo "DEBUG: menu_list_final"
	echo "Make a selection from the Office Supply Menu"
	echo ""
	echo "1. Order box of copy paper (10 reams)"
	echo "2. Order box of black pens (20 count)"
	echo "3. Order box of legal pads (15 count)"
	echo "4. Exit program"
	echo ""
	read input
	orderSelection_final
}

function orderSelection_final() {
	case $input in 
		1) 
		     	echo ""	
		     	echo "How many boxes of copy paper would you like to order? "
		     	read paper_amount
			total_price=$(echo "scale=3; $paper_amount * 24.99" | bc -l)
			echo ""
			echo "Enter department number for the order to be delivered to: "
			read department_number
			echo ""
			echo "           Order  Confirmation             "
			echo "-------------------------------------------"
			echo "An order has been created for $paper_amount boxes of copy paper."
			echo "The total price for $paper_amount box(es) of copy paper is $"$total_price"."
			echo ""
			echo "Enter (y/Y) to confirm order:"
			read confirm
			if [[ ( $confirm == y ) || ( $confirm == Y ) ]]; then
				echo "Your order has been placed."
				echo ""
				echo "End of program."
				echo ""
			fi	
		     	;;
		2)
			echo ""
			echo "How many boxes of black pens (20 count) would you like to order? " 
			read blackPens_amount
			total_price1=$(echo "scale=3; $blackPens_amount * 24.99" | bc -l)
			echo ""
			echo "Enter department number for the order to be delivered to: "
			read department_number
			echo ""
			echo "           Order  Confirmation             "
			echo "-------------------------------------------"
			echo "An order has been created for $blackPens_amount boxes of copy paper."
			echo "The total price for $blackPens_amount box(es) of copy paper is $"$total_price1"."
			echo ""
			echo "Enter (y/Y) to confirm order: "
			read confirm
			if [[ ( $confirm == y ) || ( $confirm == Y ) ]]; then
				echo ""
				echo "Your order has been placed."
				echo ""
				echo "End of program."
				echo ""
			fi	
		     	;;
		3)
			echo ""
			echo "How many packs of legal pads (15 count) would you like to order?"
			read legalPads_amount
			total_price2=$(echo "scale=3; $legalPads_amount * 24.99" | bc -l)
			echo ""
			echo "Enter department number for the order to be delivered to: "
			read department_number
			echo ""
			echo "              Order  Confirmation             "
			echo "-------------------------------------------"
			echo "An order has been created for $legalPads_amount packs of legal pads."
			echo "The total price for $legalPads_amount box(es) of copy paper is $"$total_price2"."
			echo ""
			echo "Enter (y/Y) to confirm order."
			read confirm
			if [[ ( $confirm == y ) || ( $confirm == Y ) ]]; then
				echo ""
				echo "Your order has been placed."
				echo ""
				echo "End of program."
				echo ""
			fi	
			;;
		*)
			echo ""
			echo "Invalid entry."
			echo "Program exiting..."
			echo ""
			;;
			
		esac
}

function orderSelection() {
	menu_list
	case $input in 
		1) 
		     	echo ""	
		     	echo "How many boxes of copy paper would you like to order? "
		     	read paper_amount
			total_price=$(echo "scale=3; $paper_amount * 24.99" | bc -l)
			echo ""
			echo "Enter department number for the order to be delivered to: "
			read department_number
			echo ""
			echo "-------------------------------------------"
			echo "           Order  Confirmation             "
			echo "-------------------------------------------"
			echo ""
			echo "An order has been created for $paper_amount boxes of copy paper."
			echo "The total price for $paper_amount box(es) of copy paper is $"$total_price"."
			echo ""
			echo "Press 'Y' to confirm your order."
			read confirm
			if [[ ( $confirm == y ) || ( $confirm == Y ) ]]; then
				echo ""
				echo "Your order has been placed."
				echo "End of program."
				echo ""
			else
				echo ""
				echo "Order has been cancelled."
				echo "Program exiting..."
				echo ""

			fi	
		     	;;
		2)
			echo ""
			echo "How many boxes of black pens (20 count) would you like to order? " 
			read blackPens_amount
			total_price1=$(echo "scale=3; $blackPens_amount * 24.99" | bc -l)
			echo ""
			echo "Enter department number for the order to be delivered to: "
			read department_number
			echo ""
			echo "           Order  Confirmation             "
			echo "-------------------------------------------"
			echo "An order has been created for $blackPens_amount boxes of black pens."
			echo "The total price for $blackPens_amount box(es) of black pens is $"$total_price1"."
			echo ""
			echo "Enter (y/Y) to confirm order: "
			read confirm
			if [[ ( $confirm == y ) || ( $confirm == Y ) ]]; then
				echo ""
				echo "Your order has been placed."
				echo ""
				echo "End of program."
				echo ""
			fi	
		     	;;
		3)
			echo ""
			echo "How many packs of legal pads (15 count) would you like to order?"
			read legalPads_amount1
			total_price3=$(echo "scale=3; $legalPads_amount1 * 24.99" | bc -l)
			echo ""
			echo "Enter department number for the order to be delivered to: "
			read department_number
			echo ""
			echo "           Order  Confirmation             "
			echo "-------------------------------------------"
			echo "An order has been created for $legalPads__amount1 box(es) of legal pads."
			echo "The total price for $legalPads_amount1 box(es) of legal pads is $"$total_price3"."
			echo "Enter (y/Y) to confirm order:"
			read confirm
			if [[ ( $confirm == y ) || ( $confirm == Y ) ]]; then
				echo ""	
				echo "Your order has been placed."
				echo ""	
				echo "End of program."
				echo ""
			fi	
			;;
		*)
			echo "Invalid entry."
			echo ""
			menu_list_final
			;;
			
		esac
}


# ------------------ H E A D E R -------------------------
echo ""
echo ""
echo "==============================================================="
echo ""
echo "           O F F I C E   S U P P L Y   P R O G R A M           "
echo ""
echo "==============================================================="
echo ""
echo ""
unset username
unset password

echo -n "Enter username: "
read username

prompt="Enter Passphrase: "
while IFS= read -p "$prompt" -r -s -n 1 char
do
	if [[ $char == $'\0' ]]; then
		break
	fi
	prompt="*"
	password+="$char"
done


echo ""
if [[ ( $password == 8392 ) && ( $username == "Alice" ) ]]; then
	echo ""
       	echo "Hello, Alice."
	echo ""
elif [[ ( $password == 2383 ) && ( $username == "Bob" ) ]]; then
	echo ""
	echo "Hello, Bob."
	echo ""
elif [[ ( $password == 7389 ) && ( $username == "Jane" ) ]]; then
	echo ""
	echo "Hello, Jane."
	echo ""
else
	echo "Invalid username and/or password.."
	echo ""
	authentication2
fi


orderSelection








