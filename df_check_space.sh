#!/bin/bash

source colour_coding.sh

echo ""
echo "${txtcyn}${txtbld}========================================================${txtrst}"
echo "                 ${txtcyn}${txtbld}Disk Usage Space Program ${txtrst}"
echo "${txtcyn}${txtbld}========================================================${txtrst}"
echo ""
echo "${txtcyn}${txtbld}Disk usage for this machine:${txtrst}"
df -H
echo ""
echo "${txtcyn}${txtbld}This is the amount used for the HD:${txtrst}"
echo "${txtcyn}${txtbld}===================================${txtrst}"
echo "${txtcyn}${txtbld}Use%     Filesystem     Used${txtrst}"
#df -H | grep -vE "Filesystem|tmpfs|cdrom" | awk '{ print $5 "      " $1 "        " $3}'
df -H | grep "udev" | awk '{print $5 "       " $1 "           " $3}' 
df -H | grep "/dev/sda1" | awk '{print $5 "     " $1 "      " $3}'  
