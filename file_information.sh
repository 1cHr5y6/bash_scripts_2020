#!/bin/bash

echo ""
echo ""
echo "========================================================"
echo "	           File Information Program		     " 
echo "========================================================"
echo ""
echo "Enter name of file to retrieve properties for: "
read input
echo ""
echo "             Properties for $input"
echo "             ------------------------"
	echo ""
	echo "File owner      File Size          File Name "
	ls -lh $input | awk '{ print $3 "            " $5 "                " $9 }'
	echo ""
	echo "Type is: "  
	echo "---------"
	file $input | cut -d':' -f2 
	echo ""
	echo "Inode number" 
	echo "------------"
	ls -li $input | awk '{ print $1}' 

echo ""
