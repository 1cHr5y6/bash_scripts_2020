#!/bin/bash

source mtr_headers.sh
source mtr_commands.sh
# The mtr command provides the functionality of both ping and traceroute
# The mtr command also provides load average response times

# References:
# https://en.wikipedia.org/wiki/MTR_(software)

# Clear screen
clear

# Program header
program_header

# View traceroute report in read time
mtr_basic

# Force mtr to show ip address instead of host name
mtr_ip_addr

# Have mtr show both hostname and IP address
mtr_domain_and_ip

# Have mtr exit after max pings has been reached
set_max_ping




