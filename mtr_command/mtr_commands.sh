#!/bin/bash
source colour_coding.sh

# mtr command to provide domain name or iP addr of remote machine
function mtr_basic() {
	echo ""
	echo "${txtcyn}To view the name or IP address of a site, you will${txtrst} "
	echo "${txtcyn}need to view the traceroute report in real time.${txtrst}"
	echo "             Command:"
	echo "             # mtr <site.com>"
	echo ""
	echo "${txtcyn}Enter the name or IP address of the site:${txtrst}"
	read domain
	mtr $domain
	echo ""
	read -t 5 -p "Loading exercise #2 of 4..."
	clear
}

function mtr_ip_addr() {
	echo ""
	#echo "-------------------------------------------------------"
	echo ""
	echo "${txtcyn}To force mtr to display the IP address instead of using${txtrst}"
        echo "${txtcyn}hostnames, the following command is used:${txtrst}"
	echo "             Command:"
	echo "	     #mtr -n <excite.com>"
	echo ""
	echo "${txtcyn}Enter the domain you would like to use to force ${txtrst}"
        echo "${txtcyn}mtr to display the traceroute ip addresses for:${txtrst}"	
	read domain2
	mtr -n $domain2
	echo ""
	read -t 5 -p "Loading exercise #3 of 4..." 
	clear
}

function mtr_domain_and_ip() {
	echo ""
	echo "${txtcyn}The following command will display both host name and${txtrst}"
	echo "${txtcyn}the IP address using the -b flag.${txtrst}"
	echo "             Command: "
	echo "             # mtr -b <site.com>"
	echo ""
	echo "${txtcyn}Enter the site to view both the hostname and the${txtrst}"
	echo "${txtcyn}IP address: ${txtrst}"
	read domain3
	mtr -b $domain3
	echo ""
	read -t 5 -p "Loading final exercise..."
	clear
}

function set_max_ping() {
	echo ""
	echo "${txtcyn}The following command will set the number of pings to a specific${txtrst}"
	echo "${txtcyn}value and exit mtr after max pings are met.${txtrst}"
	echo "             Command: "
	echo "             # mtr -r -c 2 <site.com> > mtr-report"

	echo ""
	echo "Generating report..."
	mtr -r -c 2 $domain > mtr-report
	echo ""
	echo "${txtcyn}==================================================${txtrst}"
	echo "${txtcyn}           R E P O R T    C O M P L E T E       ${txtrst}"
	echo "${txtcyn}==================================================${txtrst}"
	echo ""
	echo "You may now view the report titled 'mtr-report'"
	echo ""
	echo ""
	echo "${txtcyn}End of program...${txtrst}"
	echo ""
	echo ""
}
